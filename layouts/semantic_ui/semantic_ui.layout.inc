name = nws
description = A flexible 3 column Semantic UI layout By North Web Studio (http://northwebstudio.com).
preview = preview.png
template = semantic-ui-layout

; Regions
regions[branding]       = Branding
regions[navigation]     = Navigation bar
regions[fixed_nav]      = Fixed Navigation bar

regions[header]         = Header
regions[hero]           = Hero
regions[highlighted]    = Highlighted
regions[help]           = Help

regions[content_top]    = Content Top
regions[content]        = Content
regions[content_bottom] = Content Bottom

regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar

regions[footer_first]   = Footer First
regions[footer_top]   = Footer Top
regions[footer]         = Footer

; Stylesheets
stylesheets[all][] = css/layouts/semantic_ui/semantic-ui.layout.css
stylesheets[all][] = css/layouts/semantic_ui/semantic-ui.layout.no-query.css
